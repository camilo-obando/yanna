let dragged = null;

const source = document.querySelectorAll(".draggable");

source.forEach(element => {
    element.addEventListener("dragstart", (event) => {
        dragged = event.target;
    });
});

const target = document.querySelectorAll(".droptarget");
target.forEach(element => {
    element.addEventListener("dragover", (event) => {
        event.preventDefault();
    });

    element.addEventListener("drop", (event) => {
        event.preventDefault();
        element.appendChild(dragged);
        if (event.target.dataset.drop === dragged.dataset.drag) {
          console.log('yes');
        }
    });
});
